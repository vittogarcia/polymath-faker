# # Challenge 2 - Backend Developer
# It is your first day at a new job as a software engineer.You have been given a task to create a simple utility that will help other engineers generate fake data for their tests.
 
# # Requirements 
# In this task, you have to implement all the methods in the Faker class.
# letterify method should replace each question mark character (?) with any random letter
# numerify method should replace each hash character (#) with any random 0-9 number
# bothify method should do both above operations

import string
import random
from faker import Faker
from faker.providers import BaseProvider

fake_data = Faker()
string_lenght = 10
string_random = ''.join(random.choices(string.ascii_uppercase + string.digits + '#?', k = string_lenght))
def letterifyFake(string_random, data):
    return data.bothify(text='?', letters=string_random)

def numerifyFake(string_ranom, data):
    return data.bothify(text='#', letters=string_random)

def test(string_random, fake_data):
    for _ in range(5):
            print(fake_data.bothify(text='?', letters='abcdefghijklmnopqrstu?v?w?xyzABCDEFGHIJKLMNOPQRSTUVWXYZ'))

letterify = letterifyFake(string_random,fake_data)
numerify = numerifyFake(string_random, fake_data)
print(string_random)
print(letterify, numerify)